$(function() {

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
	// brand carousel init
	$('#brand-carousel').owlCarousel({
		//items
		items: 4,

		//autoplay
		autoPlay: true,
		stopOnHover: true,

		//navigation

		navigation: true,
		pagination: false,
		navigationText: ['<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>', '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>'],

		//responsive

		responsive: true,
		itemsDesctopSmall: [1024,4],
		itemsTablet: [768,4]
		


	});
	// hits carousel init
	$('#hits-carousel').owlCarousel({
		//items
		items: 1,
		singleItem: true,

		//autoplay
		autoPlay: true,
		stopOnHover: true,

		//navigation

		navigation: false,
		pagination: true,
		navigationText: ['<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>', '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>']


	});
	//slider init
	$('#slider-carousel').owlCarousel({
		//items
		items: 1,
		singleItem: true,

		//autoplay
		autoPlay: true,
		stopOnHover: true,

		//navigation

		navigation: true,
		pagination: false,
		navigationText: ['<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>', '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>']


	});

	// price slider

	$( "#catfilterSlider" ).slider({
			range: true,
			min: 0,
			max: 10000,
			values: [ 2500, 7500 ],
			slide: function( event, ui ) {
				//Поле минимального значения
				$( "#price1" ).val(ui.values[ 0 ]);
				//Поле максимального значения
				$("#price2").val(ui.values[1]);			}
		});
		//Записываем значения ползунков в момент загрузки страницы
		//То есть значения по умолчанию
		$( "#price1" ).val($( "#catfilterSlider" ).slider( "values", 0 ));
		$("#price2").val($("#catfilterSlider").slider( "values", 1 ));


	// date picker

	jQuery(function(){
 jQuery.datetimepicker.setLocale('ru');
 jQuery('#date_timepicker_start').datetimepicker({
  format:'Y/m/d',
  onShow:function( ct ){
   this.setOptions({
    maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false
   })
  },
  timepicker:false
 });
 jQuery('#date_timepicker_end').datetimepicker({
  format:'Y/m/d',
  onShow:function( ct ){
   this.setOptions({
    minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
   })
  },
  
  timepicker:false
 });
});

//modals

    var overlay = $('.overlay'); 
    var open_modal = $('.modal_open'); 
    var close = $('.close, .overlay'); 
    var modal = $('.modal');

     open_modal.click( function(event){ 
         event.preventDefault(); 
         var div = $(this).attr('href'); 
         overlay.fadeIn(400, 
             function(){ 
                 $(div) 
                     .css('display', 'block') 
                     .animate({opacity: 1, top: '50%'}, 200); 
         });
     });

     close.click( function(){ 
            modal 
             .animate({opacity: 0, top: '45%'}, 200, 
                 function(){ 
                     $(this).css('display', 'none');
                     overlay.fadeOut(400); 
                 }
             );
     });
	
// mobile menu

	var menuBtn = $('.mobile_btn');
	var menuMobile = $('nav.topnav ul')

	menuBtn.on('click', function(){
		menuMobile.slideToggle(350);
	});
			
// show filters on mobile devices
	
	var showFilter = $('.catfilter_show');
	var catFilter = $('.catfilter');

	showFilter.on('click', function(e){
		e.preventDefault();
		catFilter.slideToggle(350);
	});		

// detach price product2
	var widthScreen = $(document).width();
	var prodPricesBlock = $('.product_prices');	
	var prodCharsPos = $('.product_chars');
	if (widthScreen < 769) {
		prodPricesBlock.detach().prependTo(prodCharsPos);	
	}
	
//catmenu

	var catMenuItem = $('.catmenu ul li a');
	var catMenusubItem = $('.catmenu ul > ul li a');

	catMenuItem.on('click', this, function(e){
		e.preventDefault();
		$(this).parent().siblings('li').removeClass('open');
		$(this).parent().toggleClass('open');
		$(this).parent().next('ul').slideToggle(350);
	});		
			



});
