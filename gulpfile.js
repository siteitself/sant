var gulp         = require('gulp'),
		sass         = require('gulp-sass'),
		bourbon      = require('node-bourbon'),
		autoprefixer = require('gulp-autoprefixer'),
		cleanCSS    = require('gulp-clean-css'),
		rename       = require('gulp-rename'),
		browserSync  = require('browser-sync').create(),
		concat       = require('gulp-concat'),
		uglify       = require('gulp-uglify'),
		plumber		 = require('gulp-plumber');


gulp.task('browser-sync', ['styles', 'scripts'], function() {
		
		browserSync.init({
				server: {
						baseDir: "./app"
				},
				notify: true
		})
		
		
});

gulp.task('styles', function () {
	return gulp.src('sass/*.sass')
	.pipe(plumber())
	.pipe(sass({
		includePaths: require('node-bourbon').includePaths
	}).on('error', sass.logError))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
	.pipe(cleanCSS())
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.stream());
});

gulp.task('scripts', function() {
	return gulp.src([
		'./app/libs/modernizr/modernizr.js',
		'./app/libs/jquery/jquery-1.11.2.min.js',
		'./app/libs/waypoints/waypoints.min.js',
		'./app/libs/animate/animate-css.js',
		'./app/libs/owl.carousel/js/owl.carousel.min.js',
		'./app/libs/datepicker/js/jquery.datetimepicker.full.min.js',
		'./app/libs/jquery-ui/jquery-ui.min.js'
		])
		.pipe(plumber())
		.pipe(concat('libs.js'))
		// .pipe(uglify()) //Minify libs.js
		.pipe(gulp.dest('./app/js/'));
});

gulp.task('watch', function () {

	gulp.watch('sass/*.sass', ['styles']);
	gulp.watch('app/libs/**/*.js', ['scripts']);
	gulp.watch('app/js/*.js').on("change", browserSync.reload);
	gulp.watch('app/*.html').on('change', browserSync.reload);
	
})

gulp.task('default', ['browser-sync', 'watch'], function() {
		gulp.pipe(plumber());

	});
		